//TODO: only Gamingclient
const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require("fs");
const music = require('fun/misc/music.js');

var config = JSON.parse(fs.readFileSync('./settings.json', 'utf-8'));

const prefix = config.prefix;
const discord_token = config.discord_token;
// HERE: LOGIN!
client.login(discord_token, output);
//HERE: READY LOGIN
client.on('ready', function(ready) {
    console.log(`Logged in as: ${client.user.tag}`);
});

//HERE: output Message
function output(error, discord_token) {
    if (error) {
        console.log(`There was an error logging in: ${error}`);
        return;
    } else
        console.log(`Logged in.`);
};

//HERE: CLient destroyed!
client.destroy((err) => {
    console.log(err);
});


//Message Handler:
/*
client.on('message', function(message) {
    if (message.channel.isPrivate) {
        console.log(`(Private) ${message.author.name}: ${message.content}`);
    } else {
        console.log(`(${message.server.name} / ${message.channel.name}) ${message.author.name}: ${message.content}`);
    }
});
*/

// create an event listener for messages
client.on('message', message => {
    if (message.author.client) return;
    if (!message.content.startsWith(config.prefix)) return;

    let command = message.content.split(" ")[0]
    command = command.slice(config.prefix.length);

    let args = message.content.split(" ").slice(1);
    // if i write add then addiotion this
    if (command === "add") {
        let numArry = args.map(n => parseInt(n));
        let total = numArry.reduce((p, c) => p + c)
        message.channel.sendMessage(total).catch(console.error);
    }

    // if the message is say
    if (command === "say") {
        // sendMessage nach dem Comamnd "say"
        message.reply(args.join(" ")).catch(console.error);
    }

    // if the message is "ping",
    if (message.content.startsWith(config.prefix + "ping")) {
        // send "pong" to the same channel.
        message.send('pong!').catch(console.error);
    } else

    if (message.content.startsWith(config.prefix + "foo")) {
        let modRole = message.guild.roles.find("name", "Mods");
        if (message.member.roles.has(modRole.id)) {
            message.channel.sendMessage("bar!").catch(console.error);
        } else {
            message.reply("You bleb, you don´t have the permissions to use this Command!").catch(console.error);
        }
    }


    // TODO MUSIC!
    if (message.startsWith(config.prefix + "play") {

        })

    // TODO Avatar!
    // if the message is "what is my avatar",
        if (message.content.startsWith(config.prefix + "avatar")) {
            // send the user's avatar URL
            message.reply(message.author.avatarURL).catch(console.error);
        }

        // TODO twitter APi
        //if the Prefix + Twiter write
    if (message.content.startsWith(config.prefix + "twitter")) {
        // then send this in this channel:
        message.channel.sendMessage("Unser Twitter Acc: https://twitter.com/SoulForgeEU").catch(console.error);
    }

    // TODO TeamSpeak3 APi
    //if the Prefix + Twiter write
    if (message.content.startsWith(config.prefix + "ts")) {
        // then send this in this channel:
        message.channel.sendMessage('Ich lade euch ganz herzlich ein zu meinem Teamspeak3 Server: http://www.teamspeak.com/invite/ts3.soulforge.eu/').catch(console.error);
    }

    if (message.content.startsWith(config.prefix + "test")) {
        message.channel.send({
            embed: {
                color: 3447003,
                author: {
                    name: client.user.username,
                    icon_url: client.user.avatarURL
                },
                title: "This is an embed",
                url: "http://google.com",
                description: "This is a test embed to showcase what they look like and what they can do.",
                fields: [{
                        name: "Fields",
                        value: "They can have different fields with small headlines."
                    },
                    {
                        name: "Masked links",
                        value: "You can put [masked links](http://google.com) inside of rich embeds."
                    },
                    {
                        name: "Markdown",
                        value: "You can put all the *usual* **__Markdown__** inside of them."
                    }
                ],
                timestamp: new Date(),
                footer: {
                    icon_url: client.user.avatarURL,
                    text: "© Example"
                }
            }
        })
    };
});
// This is the function zu eval command!
function clean(text) {
    if (typeof(text) === "string")
        return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203)); 
    else    return text;
}


var embed = new Discord.RichEmbed();





//TODO Welcome Message
client.on("guildMemberAdd", member => {
    const guild = member.guild
    guild.defaultChannel.sendMessage(`Welcome ${member.user} to this Server.`).catch(console.error);
});

client.on("guildCreate", guild => {
    console.log(`New guild added : ${guild.name}, owned by $(guild.owner.user.username)`);
});


// Change Status / Check Game / He Playing a Game?
client.on("presenceUpdate", (oldMember, newMember) => {
    const guild = newMember.guild;
    const Minecraft = guild.roles.find("name", "Playing Minecraft");
    if (!Minecraft) return;

    // Play GTA Five
    const Grand_Theft_Auto_V = guild.roles.find("name", "Playing Grand_Theft_Auto_V");
    if (!Grand_Theft_Auto_V) return;

    const game = newMember.user.presence.game;
    if (game && (game.name && game.name.split(" ")[0] === "Minecraft")) {
        //  code
        newMember.addRole(Minecraft).catch(console.error);
    } else if (!game && newMember.roles.has(Minecraft.id)) {
        newMember.removeRole(Minecraft).catch(console.error);
    }
    // Play GTA Five
    if (game && (game.name && game.name.split(" ")[0] === "Grand Theft Auto V")) {
        //  code
        newMember.addRole(Grand_Theft_Auto_V).catch(console.error);
    } else if (!game && newMember.roles.has(Grand_Theft_Auto_V.id)) {
        newMember.removeRole(Grand_Theft_Auto_V).catch(console.error);
    }
});